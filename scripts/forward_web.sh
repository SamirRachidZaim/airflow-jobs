#!/usr/bin/env bash
# Set up a forward connection into the Airflow web server Pod
set -euf -o pipefail

echo "Forwarding Airflow web server to http://localhost:8080"
export POD_NAME=$(kubectl get pods --namespace default -l "component=web,app=airflow" -o jsonpath="{.items[0].metadata.name}")
kubectl port-forward --namespace default $POD_NAME 8080:8080
