# Airflow Jobs

Repository containing Airflow DAGs definition for Angostura jobs

## Develop
We've set up a separate document for [developers](DEVELOPERS.md).

## License

This project is licensed under the MIT License - see the
[LICENSE.md](LICENSE.md) file for details
