package org.codeforvenezuela.jobs.utils;

import org.apache.beam.sdk.io.Compression;
import org.apache.beam.sdk.io.FileIO;
import org.apache.beam.sdk.options.ValueProvider;
import org.apache.beam.sdk.transforms.windowing.BoundedWindow;
import org.apache.beam.sdk.transforms.windowing.IntervalWindow;
import org.apache.beam.sdk.transforms.windowing.PaneInfo;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

/**
 * FileNaming policy that names files on the format
 * $eventType/YYYY/MM/dd/${prefix}window-${isoDateTimeOfWindowStart}-pane-*.$suffix.
 */
public class ByEventTypeFileNaming implements FileIO.Write.FileNaming {
  private static final DateTimeFormatter YEAR = DateTimeFormat.forPattern("YYYY");
  private static final DateTimeFormatter MONTH = DateTimeFormat.forPattern("MM");
  private static final DateTimeFormatter DAY = DateTimeFormat.forPattern("dd");

  private final String eventType;
  private final ValueProvider<String> filePrefix;
  private final ValueProvider<String> suffix;

  private ByEventTypeFileNaming(
      String eventType, ValueProvider<String> filePrefix, ValueProvider<String> suffix) {
    this.eventType = eventType;
    this.filePrefix = filePrefix;
    this.suffix = suffix;
  }

  public static ByEventTypeFileNaming getNaming(
      String eventType, String filePrefix, String suffix) {
    return new ByEventTypeFileNaming(
        eventType,
        ValueProvider.StaticValueProvider.of(filePrefix),
        ValueProvider.StaticValueProvider.of(suffix));
  }

  public static ByEventTypeFileNaming getNaming(
      String eventType, ValueProvider<String> filePrefix, ValueProvider<String> suffix) {
    return new ByEventTypeFileNaming(eventType, filePrefix, suffix);
  }

  private String directoryForWindow(IntervalWindow window) {
    return String.format(
        "%s/%s/%s/%s",
        eventType,
        YEAR.print(window.start()),
        MONTH.print(window.start()),
        DAY.print(window.start()));
  }

  @Override
  public String getFilename(
      BoundedWindow window, PaneInfo pane, int numShards, int shardIndex, Compression compression) {
    final IntervalWindow intervalWindow = (IntervalWindow) window;
    final String directoryForWindow = directoryForWindow(intervalWindow);

    return String.format(
        "%s/%swindow-%s-to-%s-pane-%d-%s-%05d-of-%05d%s",
        directoryForWindow,
        filePrefix.get(),
        ISODateTimeFormat.dateTime().print(intervalWindow.start()),
        ISODateTimeFormat.dateTime().print(intervalWindow.end()),
        pane.getIndex(),
        pane.getTiming().toString().toLowerCase(),
        shardIndex,
        numShards,
        suffix.get());
  }
}
