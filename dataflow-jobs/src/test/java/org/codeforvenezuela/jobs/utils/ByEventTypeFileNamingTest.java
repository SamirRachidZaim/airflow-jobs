package org.codeforvenezuela.jobs.utils;

import org.apache.beam.sdk.io.Compression;
import org.apache.beam.sdk.transforms.windowing.BoundedWindow;
import org.apache.beam.sdk.transforms.windowing.IntervalWindow;
import org.apache.beam.sdk.transforms.windowing.PaneInfo;
import org.joda.time.Instant;
import org.junit.Test;

import static org.junit.Assert.*;

public class ByEventTypeFileNamingTest {
  @Test
  public void testGetFilename() {
    final ByEventTypeFileNaming naming =
        ByEventTypeFileNaming.getNaming("myEventType", "myPrefix-", ".mySuffix");
    final Instant windowStart = Instant.parse("2020-02-28T17:00:00Z");
    final Instant windowEnd = Instant.parse("2020-02-28T17:59:59Z");

    final String actualFilename =
        naming.getFilename(
            new IntervalWindow(windowStart, windowEnd),
            PaneInfo.ON_TIME_AND_ONLY_FIRING,
            10,
            1,
            Compression.GZIP);
    assertEquals(
        "myEventType/2020/02/28/myPrefix-window-2020-02-28T17:00:00.000Z-to-2020-02-28T17:59:59.000Z-pane-0-on_time-00001-of-00010.mySuffix", actualFilename);
  }
}
