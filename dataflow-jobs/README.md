## DataFlow Jobs

This project contains Dataflow pipelines used by Angostura.

### Setup

We recommend using [IntelliJ](https://www.jetbrains.com/idea/) for editing source code in this repo
as it has native integration with Maven and makes Java development a breeze. You should import an
existing project from sources and use Maven as its package manager.

To run unit tests and package the job we Maven (or `mvn`) as follows:

```bash
# Run unit tests
mvn test

# Package a JAR
mvn package

# Find the JAR assemblied with all dependencies
ls -lh target/dataflow-jobs-bundled-*
``` 

### Angostura Pubsub to GCS

Job `org.codeforvenezuela.jobs.PubsubToText` reads from Pubsub (or a file pattern for 
local development) and writes these events (partitioned by `event_type`) to a given
directory.

For a local run you could run something like the following:

```bash
export GOOGLE_APPLICATION_CREDENTIALS=$FILE_WITH_GCP_CREDENTIALS_JSON
mvn compile exec:java \
 -Dexec.mainClass=org.codeforvenezuela.jobs.PubsubToText \
 -Dexec.cleanupDaemonThreads=false \
 -Dexec.args=" \
    --project=event-pipeline \
    --runner=DirectRunner \
    --numShards=1 \
    --inputFilePattern=gs://angostura-dev/proxy-raw/*/*/*/* \
    --outputDirectory=gs://angostura-dev/proxy-raw-dev/ \
    --outputFilenamePrefix=backfill-ira \
    --outputFilenameSuffix=.json"
```

If you need to read from a PubSub topic remove `inputFilePattern` and add `inputTopic` instead.

## Deployment

We build JARs that we upload to GCS so that they can be accessed in Dataflow:

```bash
# Build the fat JAR that we need to run the Dataflow Job.
mvn package -Pdataflow-runner
# Copy it to GCS (using gsutil which comes installed with Google Cloud Utilities)
gsutil cp target/dataflow-jobs-bundled-0.1.jar gs://angostura-dataflow-templates/jars/dataflow-jobs-bundled-0.1.jar
```